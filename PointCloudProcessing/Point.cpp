#include <cmath>

#include "Point.h"

Point::Point() {}

Point::Point(double x, double y, double z) : x(x), y(y), z(z) {}

/*!
* @return The x.
*/
double Point::getX() {
	return this->x;
}

/*!
* @return The y.
*/
double Point::getY() {
	return this->y;
}

/*!
* @return The z.
*/
double Point::getZ() {
	return this->z;
}

/*!
* @param x an double argument.
*/
void Point::setX(double x) {
	this->x = x;
}

/*!
* @param y an double argument.
*/
void Point::setY(double y) {
	this->y = y;
}

/*!
* @param z an double argument.
*/
void Point::setZ(double z) {
	this->z = z;
}

/*!
* @param other an constant referenced object of the class Point.
* @return The distance.
*/
double Point::distance(const Point& other) const {
	double x = std::pow(other.x - this->x, 2);
	double y = std::pow(other.y - this->y, 2);
	double z = std::pow(other.z - this->z, 2);

	return std::sqrt(x + y + z);
}