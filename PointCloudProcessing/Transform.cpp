#include <cmath>
#define PI 3.14159265358979323846

#include "Transform.h"

Transform::Transform() {}

Transform::Transform(Eigen::Vector3d, Eigen::Vector3d trans) : angles(angles), trans(trans) {}

/*!
* @return The angles.
*/
Eigen::Vector3d Transform::getAngles() {
	return this->angles;
}

/*!
* @return The trans.
*/
Eigen::Vector3d Transform::getTrans() {
	return this->trans;
}

/*!
* @return The trans matrix.
*/
Eigen::Vector4d Transform::getTransMatrix() {
	return this->transMatrix;
}

/*!
* @param angles vector.
*/
void Transform::setAngles(Eigen::Vector3d angles) {
	this->angles = angles;
}

/*!
* @param trans vector.
*/
void Transform::setTrans(Eigen::Vector3d trans) {
	this->trans = trans;
}

/*!
* @param trans matrix vector.
*/
void Transform::setTransMatrix(Eigen::Matrix4d transMatrix) {
	this->transMatrix = transMatrix;
}

void Transform::setRotation(Eigen::Vector3d ang) {
	// Fill this in

	// Student guess: I think this is a setter function given by mistake  in the documentation to us.
	// Setter function for angles is declared and defined above.
}

void Transform::setTranslation(Eigen::Vector3d tr) {
	// Fill this in

	// Student guess: I think this is a setter function given by mistake  in the documentation to us.
	// Setter function for trans is declared and defined above.
}

/*!
* @param p an object of a class.
* @return The constructors object.
*/
Point Transform::doTransform(Point p) {
	// Fill this in

	// Camera:	(100, 500, 50)
	//			(0, 0, -90)

	// Point:	(150, 150, -50)
	//			N/A

	// Guess: Camera is our origin(Ap vector -> from documentation) so point variable is Bp vector(from documentation) for us.

	double x = this->getTrans().x();
	double y = this->getTrans().y();
	double z = this->getTrans().z();

	Point point(p.getX() - x,
		p.getY() - y,
		p.getZ() - z);

	double angle0 = ((this->getAngles().x()) / 180.0) * ((double)PI);
	double angle1 = ((this->getAngles().y()) / 180.0) * ((double)PI);
	double angle2 = ((this->getAngles().z()) / 180.0) * ((double)PI);


	std::vector<std::vector<double>> transformationMatrix = {
		{ std::cos(angle2) * std::cos(angle1) , (std::cos(angle2) * std::sin(angle1) * std::sin(angle0)) - (std::sin(angle2) * std::cos(angle0)) , (std::cos(angle2) * std::sin(angle1) * std::cos(angle0)) + (std::sin(angle2) * std::sin(angle0)), point.getX() },
		{ std::sin(angle2) * std::cos(angle1) , (std::sin(angle2) * std::sin(angle1) * std::sin(angle0)) + (std::cos(angle2) * std::cos(angle0)) , (std::sin(angle2) * std::sin(angle1) * std::cos(angle0)) - (std::cos(angle2) * std::sin(angle0)), point.getY() },
		{ -std::sin(angle1) , std::cos(angle1) * std::sin(angle0) , std::cos(angle1) * std::cos(angle0) , point.getZ() },
		{ 0 , 0 , 0 , 1 }
	};

	std::vector<double> Bp = { point.getX() , point.getY() , point.getZ() , 1 };

	std::vector<double> sumVec;

	for (auto row = 0; row < transformationMatrix.size(); row++) {
		double sum = 0;

		for (auto col = 0; col < transformationMatrix[row].size(); col++) {
			sum += transformationMatrix[row][col] * Bp[col];
		}

		sumVec.push_back(sum);
	}

	return Point(sumVec[0], sumVec[1], sumVec[2]);
}

/*!
* @param pc an object of the class PointCloud.
* @return The pointCloud object.
*/
PointCloud Transform::doTransform(PointCloud pc) {
	PointCloud pointCloud(pc.getPointNumber());

	// Transform every point based on camera angles and translations.
	for (auto point : pc.getPoints()) {
		Point p = this->doTransform(*point);
		pointCloud.addPoint(p);
	}

	return pointCloud;
}