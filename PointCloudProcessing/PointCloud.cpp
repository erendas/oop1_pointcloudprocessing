#include "PointCloud.h"

PointCloud::PointCloud() : points(std::vector<Point*>(0)), pointNumber(0) {}

PointCloud::PointCloud(int pointNumber) : pointNumber(pointNumber) {
	this->points.reserve(pointNumber);
}

PointCloud::PointCloud(std::vector<Point*> points, int pointNumber) : points(points), pointNumber(pointNumber) {}

/*!
* @return The points vector.
*/
std::vector<Point*> PointCloud::getPoints() {
	return this->points;
}

/*!
* @param point An pointer object of the class "Point".
* @param index an integer argument.
*/
void PointCloud::setPoint(Point* point, int index) {
	this->points[index] = point;
}

/*!
* @param index an integer argument.
*/
void PointCloud::erasePoint(int index) {
	auto it = this->points.begin() + index;
	this->points.erase(it);
}

/*!
* @return The pointNumber.
*/
int PointCloud::getPointNumber() {
	return this->pointNumber;
}

/*!
* @param point an vector argument.
*/
void PointCloud::setPoints(std::vector<Point*> point) {
	this->points = point;
}

/*!
* @param pointNumber an integer argument.
*/
void PointCloud::setPointNumber(int pointNumber) {
	this->pointNumber = pointNumber;
}

/*!
* @param point An object of the class "Point".
*/
void PointCloud::addPoint(Point point) {
	Point* p = new Point(point.getX(), point.getY(), point.getZ());
	this->points.push_back(p);
}