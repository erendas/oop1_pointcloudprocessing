﻿#pragma once

/**
 * @file DepthCamera.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 01.01.2022
 * @brief Get filename name from main, read from file and add the points to vector in the PointCloud.
 *
 * Open file.
 * Read file line by line.
 * Get words from line, convert them to double and add to the vector.
 * Add values to the PointCloud vector as (X, Y, Z).
 */


#include <string>
#include <fstream>

#include "PointCloud.h"
#include "PointCloudGenerator.h"

//! A reader class
/*!
* Get filename name from main, read from file and add the points to vector in the PointCloud.
*/
class DepthCamera : public PointCloudGenerator {
private:
	//! A private string.
	std::string fileName;

public:
	//! Default constructor. 
	DepthCamera();

	//! Parameterized Constructor. 
	DepthCamera(std::string fileName, FilterPipe* filterPipe, Transform transform);

public:
	//! Setter.
	void setFileName(std::string fileName);

	//! Getter.
	std::string getFileName();

public:
	//! Use this function to capture the points from file to a vector in the PointCloud.
	virtual PointCloud capture() const override;

	//! Use this function to capture the points from file to a vector in the PointCloud, impelement the filters, do transform and return the filtered points.
	virtual PointCloud captureFor() const override;

private:
	//! Get the total line count from the file.
	static int getFileLineCount(std::ifstream& file) {
		int count = 0;

		std::string unused;
		while (std::getline(file, unused)) count++;

		return count;
	}
};