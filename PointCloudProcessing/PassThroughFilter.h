﻿#pragma once

/**
 * @file PassThroughFilter.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 04.01.2022
 * @brief Checks upper and lower limits for the x,y,z values and removes the point if one of them is not between the limits.
 *
 * Get upper limits and lower limits of X, Y, Z from main.cpp.
 * Walk in the vector and if one of them(X,Y,Z) is not between the limits remove the point.
 */

#include "PointCloud.h"
#include "PointCloudFilter.h"

//! A filter class
/*!
* Checks upper and lower limits for the x,y,z values and removes the point if one of them is not between the limits.
*/
class PassThroughFilter : public PointCloudFilter {
private:
	//! A private variable.
	double upperLimitX;
	//! A private variable.
	double lowerLimitX;

	//! A private variable.
	double upperLimitY;
	//! A private variable.
	double lowerLimitY;

	//! A private variable.
	double upperLimitZ;
	//! A private variable.
	double lowerLimitZ;

public:
	//! Default constructor.
	PassThroughFilter();

	//! Parameterized Constructor.
	PassThroughFilter(
		double upperLimitX, double lowerLimitX,
		double upperLimitY, double lowerLimitY,
		double upperLimitZ, double lowerLimitZ
	);

public:
	//! Getter.
	double getUpperLimitX();

	//! Getter.
	double getLowerLimitX();

	//! Getter.
	double getUpperLimitY();

	//! Getter.
	double getLowerLimitY();

	//! Getter.
	double getUpperLimitZ();

	//! Getter.
	double getLowerLimitZ();

	//! Setter.
	void setUpperLimitX(double upperLimitX);

	//! Setter.
	void setLowerLimitX(double lowerLimitX);

	//! Setter.
	void setUpperLimitY(double upperLimitY);

	//! Setter.
	void setLowerLimitY(double lowerLimitY);

	//! Setter.
	void setUpperLimitZ(double upperLimitZ);

	//! Setter.
	void setLowerLimitZ(double lowerLimitZ);

public:
	//! A function that filters points higher or lower than the limits.
	virtual PointCloud filter(PointCloud pc);
};