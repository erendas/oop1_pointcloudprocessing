#include "PassThroughFilter.h"

PassThroughFilter::PassThroughFilter() {}

PassThroughFilter::PassThroughFilter(
	double upperLimitX, double lowerLimitX,
	double upperLimitY, double lowerLimitY,
	double upperLimitZ, double lowerLimitZ
) :
	upperLimitX(upperLimitX), lowerLimitX(lowerLimitX),
	upperLimitY(upperLimitY), lowerLimitY(lowerLimitY),
	upperLimitZ(upperLimitZ), lowerLimitZ(lowerLimitZ) {}

/*!
* @return The upper limit X.
*/
double PassThroughFilter::getUpperLimitX() {
	return this->upperLimitX;
}

/*!
* @return The lower limit X.
*/
double PassThroughFilter::getLowerLimitX() {
	return this->lowerLimitX;
}

/*!
* @return The upper limit Y.
*/
double PassThroughFilter::getUpperLimitY() {
	return this->upperLimitY;
}

/*!
* @return The lower limit Y.
*/
double PassThroughFilter::getLowerLimitY() {
	return this->lowerLimitY;
}

/*!
* @return The upper limit Z.
*/
double PassThroughFilter::getUpperLimitZ() {
	return this->upperLimitZ;
}

/*!
* @return The lower limit Z.
*/
double PassThroughFilter::getLowerLimitZ() {
	return this->lowerLimitZ;
}

/*!
* @param upperLimitX an double argument.
*/
void PassThroughFilter::setUpperLimitX(double upperLimitX) {
	this->upperLimitX = upperLimitX;
}

/*!
* @param lowerLimitX an double argument.
*/
void PassThroughFilter::setLowerLimitX(double lowerLimitX) {
	this->lowerLimitX = lowerLimitX;
}

/*!
* @param upperLimitY an double argument.
*/
void PassThroughFilter::setUpperLimitY(double upperLimitY) {
	this->upperLimitY = upperLimitY;
}

/*!
* @param lowerLimitY an double argument.
*/
void PassThroughFilter::setLowerLimitY(double lowerLimitY) {
	this->lowerLimitY = lowerLimitY;
}

/*!
* @param upperLimitZ an double argument.
*/
void PassThroughFilter::setUpperLimitZ(double upperLimitZ) {
	this->upperLimitZ = upperLimitZ;
}

/*!
* @param lowerLimitZ an double argument.
*/
void PassThroughFilter::setLowerLimitZ(double lowerLimitZ) {
	this->lowerLimitZ = lowerLimitZ;
}

/*!
* @param pc an object of the class PointCloud.
* @return The pc object.
*/
PointCloud PassThroughFilter::filter(PointCloud pc) {
	for (int i = 0; i < pc.getPoints().size(); i++) {
		if (pc.getPoints()[i]->getX() >= this->getUpperLimitX() ||
			pc.getPoints()[i]->getX() <= this->getLowerLimitX() ||
			pc.getPoints()[i]->getY() >= this->getUpperLimitY() ||
			pc.getPoints()[i]->getY() <= this->getLowerLimitY() ||
			pc.getPoints()[i]->getZ() >= this->getUpperLimitZ() ||
			pc.getPoints()[i]->getZ() <= this->getLowerLimitZ()) {
			pc.erasePoint(i);
		}
	}

	return pc;
}