﻿#pragma once

/**
 * @file PointCloudGenerator.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 12.01.2022
 * @brief An abstract class for DepthCamera class.
 */

#include "Transform.h"
#include "FilterPipe.h"

 //! A abstract class
/*!
* An abstract class for DepthCamera class.
*/
class PointCloudGenerator {
private:
	//! A private object.
	Transform transform;

	//! A private object.
	FilterPipe* filterPipe;

public:
	//! Default constructor. 
	PointCloudGenerator();

	//! Parameterized Constructor.
	PointCloudGenerator(Transform transform, FilterPipe* filterPipe);

	//! Getter.
	Transform getTransform() const;

	//! Getter.
	FilterPipe* getFilterPipe() const;

	//! A pure virtual funciton for capture function.
	virtual PointCloud capture() const = 0;

	//! A pure virtual funciton for captureFor function.
	virtual PointCloud captureFor() const = 0;

	//! Setter.
	void setFilterPipe(FilterPipe* filterPipe);
};