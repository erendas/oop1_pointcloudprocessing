#include <iostream>
#include <fstream>
#include <sstream>

#include "DepthCamera.h"
#include "RadiusOutlierFilter.h"
#include "PassThroughFilter.h"

DepthCamera::DepthCamera() {}

DepthCamera::DepthCamera(std::string fileName, FilterPipe* filterPipe, Transform transform) : PointCloudGenerator(transform, filterPipe), fileName(fileName) {
}

/*!
* @param fileName an string argument.
*/
void DepthCamera::setFileName(std::string fileName) {
	this->fileName = fileName;
}

/*!
* @return The fileName.
*/
std::string DepthCamera::getFileName() {
	return this->fileName;
}

/*!
* @return The pointCloud object.
*/
PointCloud DepthCamera::capture() const {
	char DELIMITER = ' ';

	int row = 0;

	std::ifstream file(fileName);
	if (!file.is_open()) {
		std::cout << "Error occured while opening file... Check your file and start again...\n";
		std::cout << "Exiting from the program...\n";
		exit(1);
	}

	PointCloud pointCloud(this->getFileLineCount(file));

	file.clear();
	file.seekg(0);

	std::string line;
	while (std::getline(file, line)) {
		std::vector<double> points;

		std::istringstream iss(line);

		for (std::string word; iss >> word; ) {
			points.push_back(std::stod(word));
		}

		pointCloud.addPoint(Point(points[0], points[1], points[2]));
	}

 	file.close();

	return pointCloud;
}

/*!
* @return The finalised point cloud.
*/
PointCloud DepthCamera::captureFor() const {
	char DELIMITER = ' ';

	int row = 0;

	std::ifstream file(fileName);
	if (!file.is_open()) {
		std::cout << "Error occured while opening file... Check your file and start again...\n";
		std::cout << "Exiting from the program...\n";
		exit(1);
	}

	PointCloud pointCloud(this->getFileLineCount(file));

	file.clear();
	file.seekg(0);

	std::string line;
	while (std::getline(file, line)) {
		std::vector<double> points;

		std::istringstream iss(line);

		for (std::string word; iss >> word; ) {
			points.push_back(std::stod(word));
		}

		pointCloud.addPoint(Point(points[0], points[1], points[2]));
	}

	file.close();

	FilterPipe* filterPipe = this->getFilterPipe();
	pointCloud = filterPipe->filterOut(pointCloud);

	Transform transform = this->getTransform();
	pointCloud = transform.doTransform(pointCloud);

	return pointCloud;
}