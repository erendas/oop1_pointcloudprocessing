﻿#pragma once

/**
 * @file PointCloudInterface.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 16.01.2022
 * @brief A class that simplifies steps.
 */

#include "PointCloud.h"
#include "PointCloudGenerator.h"
#include "PointCloudRecorder.h"

 //! A simplifying class
 /*!
   * A class that simplifies steps.
   */
class PointCloudInterface {
private:
	//! A private object.
	PointCloud pointCloud;

	//! A private object.
	PointCloud patch;

	//! A private vector.
	std::vector<PointCloudGenerator*> generators;

	//! A private object.
	PointCloudRecorder* recorder;

public:
	//! Adds generator.
	void addGenerator(PointCloudGenerator* pointCloudGenerator);

	//! Setter.
	void setRecorder(PointCloudRecorder* pointCloudRecorder);

	//! Calls captureFor funciton for all generators.
	bool generate();

	//! Records points in the point cloud to file.
	bool record();
};