﻿#pragma once

/**
 * @file PointCloud.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 02.01.2022
 * @brief Holds points in the vector and with operator overloading helps the code
 *
 * Holds the points in the vector
 * + operator adds two point vectors to one final vector
 * = operator copys a point to another point.
 */

#include <vector>
#include <set>

#include "Point.h"

 //! A PointCloud class
  /*!
  * Holds points in the vector and with operator overloading helps the code
  */
class PointCloud {
private:
	//! A private vector.
	std::vector<Point*> points;

	//! A private variable.
	int pointNumber;

public:
	//! Default constructor.
	PointCloud();

	//! Parameterized Constructor. 
	PointCloud(int pointNumber);

	//! Parameterized Constructor. 
	PointCloud(std::vector<Point*> points, int pointNumber);

public:
	/*!
	* Use this operator to add two point vectors to one final vector.
	* @param other An referenced constant object of the class "PointCloud".
	* @return The constructors object. 
	*/
	PointCloud operator+(const PointCloud& other) {
		std::set<Point*> set;

		for (auto otherItem : other.points) {
			for (auto thisItem : this->getPoints()) {
				if (thisItem->operator==(*otherItem)) {
					continue;
				}
				else {
					set.insert(thisItem);
				}
			}
			set.insert(otherItem);
		}

		return PointCloud(std::vector<Point*>(set.begin(), set.end()), set.size());
	}

	//! Use this operator to copy a point to another point.
	/*!
	* @param other An referenced constant object of the class "PointCloud".
	*/
	void operator=(const PointCloud& other) {
		this->setPoints(other.points);
		this->setPointNumber(other.pointNumber);
	}

public:
	//! Getter.
	std::vector<Point*> getPoints();

	//! Getter.
	int getPointNumber();

	//! Setter.
	void setPoints(std::vector<Point*> point);

	//! Setter.
	void setPoint(Point* point, int index);

	//! A function that erases point.
	void erasePoint(int index);

	//! Setter.
	void setPointNumber(int pointNumber);

	//! A function that add points to the vector.
	void addPoint(Point point);
};