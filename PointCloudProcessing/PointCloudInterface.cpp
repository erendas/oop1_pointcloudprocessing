#include <iostream>

#include "PointCloudInterface.h"

/*!
* @param pointCloudGenerator An pointer object of the class "PointCloudGenerator".
*/
void PointCloudInterface::addGenerator(PointCloudGenerator* pointCloudGenerator) {
	this->generators.push_back(pointCloudGenerator);
}

/*!
* @param pointCloudRecorder An pointer object of the class "PointCloudRecorder"
*/
void PointCloudInterface::setRecorder(PointCloudRecorder* pointCloudRecorder) {
	this->recorder = pointCloudRecorder;
}

/*!
* @return true
*/
bool PointCloudInterface::generate() {
	for (auto& generator : this->generators) {
		PointCloud pc = generator->captureFor();
		this->pointCloud = this->pointCloud + pc;
	}

	return true;
}

/*!
* @return true
*/
bool PointCloudInterface::record() {
	this->recorder->save(this->pointCloud);

	return true;
}