﻿#pragma once

#include "PointCloud.h"

/**
 * @file PointCloudFilter.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 09.01.2022
 * @brief Abstract class for filters.
 *
 */

 //! A filter class
   /*!
   * Abstract class for filters.
   */
class PointCloudFilter {
public:
	//! A pure virtual funciton for filter function.
	virtual PointCloud filter(PointCloud pc) = 0;
};