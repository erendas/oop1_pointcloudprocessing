#include "FilterPipe.h"

FilterPipe::FilterPipe() {

}

/*!
* @param filter An pointer object of the class "PointCloudFilter".
*/
void FilterPipe::addFilter(PointCloudFilter* filter) {
	this->filters.push_back(filter);
}

/*!
* @param pc An object of the class "PointCloud".
* @return pc object.
*/
PointCloud FilterPipe::filterOut(PointCloud pc) {
	for (auto& filter : this->filters) {
		pc = filter->filter(pc);
	}

	return pc;
}