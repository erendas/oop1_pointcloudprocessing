#include "RadiusOutlierFilter.h"

RadiusOutlierFilter::RadiusOutlierFilter() {}

RadiusOutlierFilter::RadiusOutlierFilter(double radius): radius(radius) {}

/*!
* @return The radius.
*/
double RadiusOutlierFilter::getRadius() {
	return this->radius;
}

/*!
* @param radius an double argument.
*/
void RadiusOutlierFilter::setRadius(double radius) {
	this->radius = radius;
}

/*!
* @param pc an referenced object of the class PointCloud.
* @return The pc object.
*/
PointCloud RadiusOutlierFilter::filter(PointCloud pc) {
	for (int i = 0; i < pc.getPoints().size(); i++) {
		int count = 0;

		for (auto y : pc.getPoints()) {

			// if distance between base point and iterated point is lower than radius, increment count
			// if count = 0 that means there are no distance is lower than radius
			// and we should delete base point
			double value = pc.getPoints()[i]->distance(*y);
			if (value < this->getRadius()) {
				if (pc.getPoints()[i] == y) {
					continue;
				}
				else {
					count++;
				}
			}
		}

		if (count == 0) {
			pc.erasePoint(i);
		}
	}

	return pc;
}