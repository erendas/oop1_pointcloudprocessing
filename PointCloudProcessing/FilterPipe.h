﻿#pragma once

/**
 * @file FilterPipe.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 11.01.2022
 * @brief This class will filter the point cloud that needs to pass through more than one filter.
 *
 * Filters will be added with addFilter funciton.
 * Point cloud will be filtered according to add order.
 * Result will return the point cloud.
 */

#include "PointCloudFilter.h"

 //! A filter class
 /*!
  * This class will filter the point cloud that needs to pass through more than one filter.
  */
class FilterPipe {
private:
	//! A private vector.
	std::vector<PointCloudFilter *> filters;

public:
	//! Default constructor.
	FilterPipe();

	//! Adds different type of filters or adds same type of filters with different parameters.
	void addFilter(PointCloudFilter* filter);

	//! Filters point cloud according to order and returns the result.
	PointCloud filterOut(PointCloud pc);
};