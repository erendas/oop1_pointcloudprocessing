﻿#pragma once

/**
 * @file PointCloudRecorder.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 02.01.2022
 * @brief Get filename name from main, write the points to the file.
 *
 * Open file.
 * Write to file line by line.
 */

#include <string>

#include "PointCloud.h"

 //! A writer class
 /*!
 * Get filename name from main, write the points to the file.
 */
class PointCloudRecorder {
private:
	//! A private string.
	std::string fileName;

public:
	//! Default constructor.
	PointCloudRecorder();

	//! Parameterized Constructor.
	PointCloudRecorder(std::string fileName);

public:
	//! Getter.
	std::string getFileName();

	//! Setter.
	void setFileName(std::string fileName);

public:
	//! A function to save the points from the final vector to the txt file.
	bool save(PointCloud& pc);
};