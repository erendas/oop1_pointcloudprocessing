#include "DepthCamera.h"
#include "RadiusOutlierFilter.h"
#include "PassThroughFilter.h"
#include "Transform.h"
#include "PointCloudRecorder.h"
#include "PointCloudInterface.h"

int main(void) {

	const std::string output("output.txt");
	PointCloudRecorder* pointCloudRecorder = new PointCloudRecorder(output);

	FilterPipe* filterPipeForCamera1 = new FilterPipe();
	filterPipeForCamera1->addFilter(new RadiusOutlierFilter(25));
	filterPipeForCamera1->addFilter(new PassThroughFilter(400, 0, 400, 0, 45, -45));

	Transform transformForCamera1(Eigen::Vector3d(0, 0, -90), Eigen::Vector3d(100, 500, 50));

	PointCloudGenerator* pointCloudGeneratorForCamera1 = new DepthCamera("camera1.txt", filterPipeForCamera1, transformForCamera1);

	// Camera 2
	FilterPipe* filterPipeForCamera2 = new FilterPipe();
	filterPipeForCamera2->addFilter(new RadiusOutlierFilter(25));
	filterPipeForCamera2->addFilter(new PassThroughFilter(500, 0, 500, 0, 45, -45));

	Transform transformForCamera2(
		Eigen::Vector3d(0, 0, 90),
		Eigen::Vector3d(550, 150, 50)
	);

	PointCloudGenerator* pointCloudGeneratorForCamera2 = new DepthCamera("camera2.txt", filterPipeForCamera2, transformForCamera2);

	PointCloudInterface pointCloudInterface;

	pointCloudInterface.setRecorder(pointCloudRecorder);

	pointCloudInterface.addGenerator(pointCloudGeneratorForCamera1);
	pointCloudInterface.addGenerator(pointCloudGeneratorForCamera2);

	pointCloudInterface.generate();

	pointCloudInterface.record();

	return 0;
}