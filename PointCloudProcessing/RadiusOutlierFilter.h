﻿#pragma once

/**
 * @file RadiusOutlierFilter.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 05.01.2022
 * @brief Looking at the given radius value and one point, checks if any other points distance closer than radius value.
 *
 * Select one point.
 * Look at the distances with other points.
 * If all of the points are farther than radius value.
 * Erase the point.
 */

#include "PointCloud.h"
#include "PointCloudFilter.h"

//! A filter class
/*!
* Looking at the given radius value and one point, checks if any other points distance closer than radius value.
*/
class RadiusOutlierFilter : public PointCloudFilter {
private:
	//! A private variable.
	double radius;

public:
	//! Default constructor.
	RadiusOutlierFilter();

	//! Parameterized Constructor. 
	RadiusOutlierFilter(double radius);

public:
	//! Getter.
	double getRadius();
	
	//! Setter.
	void setRadius(double radius);

public:
	//! A function to filter isolated points.
	virtual PointCloud filter(PointCloud pc);
};