﻿#pragma once

/**
 * @file Transform.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 06.01.2022
 * @brief To find a Ap vector using trans matrix.
 *
 * Get angles and trans from main function
 * Using cameras origin and point as Bp we use trans matrix.
 * Finding Ap vector
 * Create new point with Ap vectors X,Y,Z
 */

#include <vector>

#include "PointCloud.h"

#include <Eigen/Dense>

 //! A transform class
   /*!
   * To find a Ap vector using trans matrix.
   */
class Transform {
private:
	//! A private vector.
	Eigen::Vector3d angles;

	//! A private vector.
	Eigen::Vector3d trans;

	//! A private matrix.
	Eigen::Matrix4d transMatrix;

public:
	//! Default constructor.
	Transform();

	//! Parameterized Constructor.
	Transform(Eigen::Vector3d angles, Eigen::Vector3d trans);

public:
	//! Getter.
	Eigen::Vector3d getAngles();

	//! Getter.
	Eigen::Vector3d getTrans();

	//! Getter.
	Eigen::Vector4d getTransMatrix();

	//! Setter.
	void setAngles(Eigen::Vector3d angles);

	//! Setter.
	void setTrans(Eigen::Vector3d trans);

	//! Setter.
	void setTransMatrix(Eigen::Matrix4d transMatrix);

public:
	//! Setter.
	void setRotation(Eigen::Vector3d ang);

	//! Setter.
	void setTranslation(Eigen::Vector3d tr);

	//! Use trans martix to find Ap.
	Point doTransform(Point p);

	//! Do the transform and add the point to the PointCloud vector.
	PointCloud doTransform(PointCloud pc);
};