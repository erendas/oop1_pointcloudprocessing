#include <iostream>
#include <fstream>

#include "PointCloudRecorder.h"

PointCloudRecorder::PointCloudRecorder() {}

PointCloudRecorder::PointCloudRecorder(std::string fileName) : fileName(fileName) {}

/*!
* @return The fileName.
*/
std::string PointCloudRecorder::getFileName() {
	return this->fileName;
}

/*!
* @param fileName an string argument.
*/
void PointCloudRecorder::setFileName(std::string fileName) {
	this->fileName = fileName;
}

/*!
* @return True or false.
*/
bool PointCloudRecorder::save(PointCloud& pc) {
	std::ofstream file(this->getFileName());
	if (!file.is_open()) {
		std::cout << "Error occured while opening file... Check your file....\n";
		return false;
	}

	for (auto item : pc.getPoints()) {
		file << item->getX() << "   " << item->getY() << "   " << item->getZ() << '\n';
	}

	file.close();

	return true;
}