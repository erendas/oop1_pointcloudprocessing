#include "PointCloudGenerator.h"

PointCloudGenerator::PointCloudGenerator() {}

PointCloudGenerator::PointCloudGenerator(Transform transform, FilterPipe* filterPipe) : transform(transform), filterPipe(filterPipe) {}

/*!
* @return The transform object.
*/
Transform PointCloudGenerator::getTransform() const { return this->transform; }

/*!
* @return The filterPipe object.
*/
FilterPipe* PointCloudGenerator::getFilterPipe() const { return this->filterPipe; }

/*!
* @param filterPipe An pointer object of the class "FilterPipe".
*/
void PointCloudGenerator::setFilterPipe(FilterPipe* filterPipe) {
	this->filterPipe = filterPipe;
}