﻿#pragma once

/**
 * @file Point.h
 * @Author Eren Daş (erendasss@gmail.com)
 * @date 01.01.2022
 * @brief Creates a point with X,Y,Z values and checks if any 2 points from the cloud identical.
 *
 * Creates a point with X,Y,Z values
 * Checks if any 2 points from the cloud identical
 * Calculates the distance between 2 points.
 */

 //! A Point class
 /*!
 * Creates a point with X,Y,Z values and checks if any 2 points from the cloud identical.
 */
class Point {
private:
	//! A private variable.
	double x;
	//! A private variable.
	double y;
	//! A private variable.
	double z;

public:
	//! Default constructor.
	Point();

	//! Parameterized Constructor. 
	Point(double x, double y, double z);

public:

	//! Use this operator to find points that identical.
	bool operator==(const Point& other) {
		return this->x == other.x && this->y == other.y && this->z == other.z;
	}

public:
	//! Getter.
	double getX();

	//! Getter.
	double getY();

	//! Getter.
	double getZ();

	//! Setter.
	void setX(double x);

	//! Setter.
	void setY(double y);

	//! Setter.
	void setZ(double z);

public:
	//! A function to find the distance between two points.
	double distance(const Point&) const;
};